output "instance_1_external_ip" {
  value       = google_compute_instance.vm_1.network_interface[0].access_config[0].nat_ip
  description = "External IP of the first instance."
}

output "instance_2_external_ip" {
  value       = google_compute_instance.vm_2.network_interface[0].access_config[0].nat_ip
  description = "External IP of the second instance."
}

output "cloudsql_instance_name" {
  value       = google_sql_database_instance.mysql.name
  description = "Cloud SQL instance name."
}

output "cloudsql_instance_connection_name" {
  value       = google_sql_database_instance.mysql.connection_name
  description = "Cloud SQL instance connection name."
}
