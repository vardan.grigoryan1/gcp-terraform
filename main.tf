resource "google_sql_database_instance" "mysql" {
  name             = var.database_name
  region           = var.region
  database_version = "MYSQL_8_0"
  deletion_protection = false

  settings {
    tier = "db-f1-micro"
    ip_configuration {
      private_network = google_service_networking_connection.private_vpc_connection.network
    }
  }
}

resource "google_sql_database" "db" {
  name      = var.database_name
  instance  = google_sql_database_instance.mysql.name
  collation = "utf8_general_ci"
}

resource "google_sql_user" "user" {
  name     = var.database_username
  instance = google_sql_database_instance.mysql.name
  password = var.database_password
}
resource "google_compute_instance" "vm_2" {
  depends_on = [google_sql_database_instance.mysql]
  name         = var.instance_name_2
  machine_type = var.instance_type
  zone         = var.zone
  tags         = ["vm2"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet.self_link
    network_ip = google_compute_address.vm_2_internal_ip.address

    access_config {
      // Ephemeral external IP
    }
  }
  metadata = {
    ssh-keys = "onboarddev7:ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBGrzDiLmUHKHETpFqXejuoNjecQN+MDw8qW2Jpm/1mW9cygG3Rp7liKdlep+3Vb1NxOCINRZgRXAZ1CwZjGndLw= google-ssh {\"userName\":\"onboarddev7@gmail.com\",\"expireOn\":\"2023-04-13T04:19:15+0000\"}\nonboarddev7:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAFKasdUBSWmAsfAp3S7q7DlZr3T2T4eFfa2RHQ+0sVKFaMRKpoTP8vlA5YH6GgDx10hXlolCPcJBlEuyWeLvqmBvXu+/OxyreXeA3r63EtHbsDxQEHti4NjydSB7aVYuZt3Ex88gzqxpEbXuhpJL7JsX9rR6Ecv6UoqB9CW5e4W3rTbYqg7GuvvMk9jkvlmKM2Ic3IWCIi0BfBkqUsmWEUJtKLNh8t2JNM7HGy+2Yrnh499zPQGDRAAbgTTcCTLkVg8wbGXGY28DEOFMiiMVpaDeenUIh42XauuVmspubrT08IMOsJlB+IHSDWSuRnhNgZtFYTlNILUVImbxy9UN238= google-ssh {\"userName\":\"onboarddev7@gmail.com\",\"expireOn\":\"2023-04-13T04:19:30+0000\"}"
  }
  metadata_startup_script = <<-EOT
#!/bin/bash
sudo apt-get update
sudo apt-get install -y curl gnupg
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs git nginx mariadb-client
sudo npm install --global yarn

cd /opt
git clone https://github.com/ditsmod/realworld.git
cd realworld

# Create the .env file with correct values
cat > /opt/realworld/packages/server/.env << EOF
NODE_ENV=dev
MYSQL_HOST=${google_sql_database_instance.mysql.private_ip_address}
MYSQL_PORT=3306
MYSQL_USER=${var.database_username}
MYSQL_DATABASE=${var.database_name}
MYSQL_CHARSET=utf8mb4
MYSQL_PASSWORD=${var.database_password}
LOGS_DIR=
CRYPTO_SECRET=here-secret
JWT_SECRET=here-secret
EOF

# MySql restore
mysql --host=${google_sql_database_instance.mysql.private_ip_address} --user=${var.database_username} --password=${var.database_password} ${var.database_name} < /opt/realworld/packages/server/sql/dump/info.sql

sudo yarn install

# Configure Nginx
sudo cat > /etc/nginx/sites-available/default << NGINXCONF
server {
  listen 3001;
  server_name _;
location /api/ {
     proxy_pass http://localhost:3000;
    proxy_http_version 1.1;
    proxy_set_header Upgrade \$http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host \$host;
    proxy_cache_bypass \$http_upgrade;
    add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE' always;
    add_header 'Access-Control-Allow-Headers' 'Authorization, Content-Type, Accept, Origin, User-Agent, Cache-Control, Keep-Alive, If-Modified-Since, X-Requested-With' always;
}
}
NGINXCONF

sudo systemctl restart nginx

nohup sudo yarn start 2>&1 &

EOT

}

data "google_compute_instance" "vm_2" {
  name = var.instance_name_2
  zone = var.zone
}

resource "google_compute_instance" "vm_1" {
  depends_on = [google_compute_instance.vm_2]
  name         = var.instance_name_1
  machine_type = var.instance_type
  zone         = var.zone
  tags         = ["vm1"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet.self_link
    access_config {
      // Ephemeral external IP
    }
  }
  metadata = {
    ssh-keys = "onboarddev7:ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBGrzDiLmUHKHETpFqXejuoNjecQN+MDw8qW2Jpm/1mW9cygG3Rp7liKdlep+3Vb1NxOCINRZgRXAZ1CwZjGndLw= google-ssh {\"userName\":\"onboarddev7@gmail.com\",\"expireOn\":\"2023-04-13T04:19:15+0000\"}\nonboarddev7:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAFKasdUBSWmAsfAp3S7q7DlZr3T2T4eFfa2RHQ+0sVKFaMRKpoTP8vlA5YH6GgDx10hXlolCPcJBlEuyWeLvqmBvXu+/OxyreXeA3r63EtHbsDxQEHti4NjydSB7aVYuZt3Ex88gzqxpEbXuhpJL7JsX9rR6Ecv6UoqB9CW5e4W3rTbYqg7GuvvMk9jkvlmKM2Ic3IWCIi0BfBkqUsmWEUJtKLNh8t2JNM7HGy+2Yrnh499zPQGDRAAbgTTcCTLkVg8wbGXGY28DEOFMiiMVpaDeenUIh42XauuVmspubrT08IMOsJlB+IHSDWSuRnhNgZtFYTlNILUVImbxy9UN238= google-ssh {\"userName\":\"onboarddev7@gmail.com\",\"expireOn\":\"2023-04-13T04:19:30+0000\"}"
  }
  metadata_startup_script = <<-EOT
    #!/bin/bash
    sudo apt-get update
    sudo apt-get install -y curl gnupg
    curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
    sudo apt-get install -y nodejs git
    cd /opt
    git clone https://github.com/khaledosman/react-redux-realworld-example-app.git
    cd react-redux-realworld-example-app

    # Change API URL
    sed -i "s|https://conduit.productionready.io/api|http://${google_compute_instance.vm_2.network_interface[0].access_config[0].nat_ip}:3001/api|g" src/agent.js
   
    sudo npm install
    sudo npm install history
    nohup npm run start 2>&1 &
  EOT
}
