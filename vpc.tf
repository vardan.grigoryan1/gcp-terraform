resource "google_compute_network" "vpc" {
  name                    = var.vpc_name
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "subnet" {
  name          = var.subnet_name
  ip_cidr_range = "10.0.0.0/16"
  region        = var.region
  network       = google_compute_network.vpc.self_link
}
resource "google_compute_global_address" "private_ip_address" {
  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.vpc.self_link
}

resource "google_compute_address" "vm_2_internal_ip" {
  name         = "vm-2-internal-ip"
  subnetwork   = google_compute_subnetwork.subnet.self_link
  address_type = "INTERNAL"
  region       = var.region
}
resource "google_service_networking_connection" "private_vpc_connection" {
  network                 = google_compute_network.vpc.self_link
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}
resource "google_compute_firewall" "vm1_allow_ports" {
  name    = "vm1-allow-ports"
  network = google_compute_network.vpc.name

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "4100"]
  }

  target_tags   = ["vm1"]
  source_ranges = ["0.0.0.0/0"]
}
resource "google_compute_firewall" "vm2_allow_ports" {
  name    = "vm2-allow-ports"
  network = google_compute_network.vpc.name

  allow {
    protocol = "tcp"
    ports    = ["22", "3001"]
  }

  target_tags   = ["vm2"]
  source_ranges = ["0.0.0.0/0"]
}
