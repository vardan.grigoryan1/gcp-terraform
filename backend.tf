terraform {
  backend "gcs" {
    bucket = "itsvit-terraform"
    prefix = "terraform/state"
  }
}