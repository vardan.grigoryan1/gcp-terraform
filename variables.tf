variable "project_id" {
  description = "The Google Cloud project ID."
  default     = "skillful-cider-378411"
}

variable "region" {
  description = "The Google Cloud region to deploy resources."
  default     = "us-central1"
}

variable "zone" {
  description = "The Google Cloud zone to deploy resources."
  default     = "us-central1-a"
}

variable "vpc_name" {
  description = "The VPC name."
  default     = "my-vpc"
}

variable "subnet_name" {
  description = "The subnet name."
  default     = "my-subnet"
}

variable "instance_name_1" {
  description = "The first instance name."
  default     = "vm-frontend"
}

variable "instance_name_2" {
  description = "The second instance name."
  default     = "vm-backend"
}

variable "instance_type" {
  description = "The instance type."
  default     = "e2-standard-8"
}

variable "database_name" {
  description = "The Cloud SQL database name."
  default     = "realworld"
}

variable "database_username" {
  description = "The Cloud SQL database username."
  default     = "realworld"
}

variable "database_password" {
  description = "The Cloud SQL database password."
  default     = "here-secret"
}
